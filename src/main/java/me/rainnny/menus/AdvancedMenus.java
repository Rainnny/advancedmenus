package me.rainnny.menus;

import me.rainnny.api.annotation.AutoLoad;
import org.bukkit.plugin.java.JavaPlugin;

/*
 * Created by Braydon on 3/23/20 5:39 a.m.
 */
@AutoLoad
public class AdvancedMenus extends JavaPlugin {
    public static AdvancedMenus INSTANCE;

    @Override
    public void onEnable() {
        INSTANCE = this;
    }
}
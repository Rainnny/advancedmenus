package me.rainnny.menus.menu.action.impl;

import me.rainnny.api.util.Style;
import me.rainnny.menus.menu.action.ItemAction;
import org.bukkit.craftbukkit.libs.joptsimple.internal.Strings;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
public class PlayerMessageAction extends ItemAction {
    public PlayerMessageAction() {
        super("message");
    }

    @Override
    public void run() {
        getPlayer().sendMessage(Style.color(Strings.join(getArgs(), " ")));
    }
}
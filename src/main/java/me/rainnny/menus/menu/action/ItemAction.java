package me.rainnny.menus.menu.action;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
@Getter
public abstract class ItemAction {
    private final String name;
    @Setter private Player player;
    @Setter private String[] args;

    public ItemAction(String name) {
        this.name = name;
    }

    public abstract void run();
}
package me.rainnny.menus.menu.action.impl;

import me.rainnny.api.menu.AtlasMenu;
import me.rainnny.api.util.Tuple;
import me.rainnny.menus.menu.Menu;
import me.rainnny.menus.menu.MenuManager;
import me.rainnny.menus.menu.MenuProvider;
import me.rainnny.menus.menu.action.ItemAction;

import java.util.AbstractMap;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
public class CloseMenuAction extends ItemAction {
    public CloseMenuAction() {
        super("closeMenu");
    }

    @Override
    public void run() {
        Tuple<AtlasMenu, Menu> menu = MenuProvider.getOpenedMenu(getPlayer());
        if (menu != null)
            menu.one.close();
    }
}
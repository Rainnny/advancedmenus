package me.rainnny.menus.menu.action;

import lombok.Setter;
import me.rainnny.api.annotation.Init;
import me.rainnny.menus.menu.action.impl.BungeeAction;
import me.rainnny.menus.menu.action.impl.CloseMenuAction;
import me.rainnny.menus.menu.action.impl.PlayerMessageAction;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
@Init
public class ActionManager {
    @Setter private static final List<ItemAction> actions = new ArrayList<>();

    public ActionManager() {
        actions.add(new PlayerMessageAction());
        actions.add(new BungeeAction());
        actions.add(new CloseMenuAction());
    }

    public static void executeAction(Player player, String s) {
        if (s.isEmpty())
            return;
        if (!s.contains("[") || !s.contains("]"))
            return;
        for (ItemAction action : actions) {
            if (action.getName().equalsIgnoreCase(s.split("]")[0].replace("[", ""))) {
                action.setPlayer(player);
                if (s.contains("] "))
                    action.setArgs(s.split("] ")[1].split(" "));
                else action.setArgs(new String[0]);
                action.run();
                break;
            }
        }
    }
}
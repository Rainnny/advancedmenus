package me.rainnny.menus.menu.action.impl;

import me.rainnny.api.util.BungeeUtils;
import me.rainnny.api.util.Style;
import me.rainnny.menus.menu.action.ItemAction;
import org.bukkit.craftbukkit.libs.joptsimple.internal.Strings;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
public class BungeeAction extends ItemAction {
    public BungeeAction() {
        super("bungee");
    }

    @Override
    public void run() {
        BungeeUtils.sendToServer(getPlayer(), getArgs()[0]);
    }
}
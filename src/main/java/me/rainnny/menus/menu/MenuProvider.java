package me.rainnny.menus.menu;

import me.rainnny.api.menu.AtlasMenu;
import me.rainnny.api.menu.Button;
import me.rainnny.api.util.BungeeUtils;
import me.rainnny.api.util.ItemBuilder;
import me.rainnny.api.util.Style;
import me.rainnny.api.util.Tuple;
import me.rainnny.menus.menu.action.ActionManager;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
public class MenuProvider extends AtlasMenu {
    private static final HashMap<Player, MenuProvider> menus = new HashMap<>();

    private final Menu menu;

    public MenuProvider(Player player, String title, int size, Menu menu) {
        super(player, Style.color(title
                .replaceAll("%player%", player.getName())), size, true);
        this.menu = menu;
    }

    @Override
    public void init() {
        menus.put(getPlayer(), this);
        for (MenuItem item : menu.getItems()) {
            List<String> lore = new ArrayList<>();
            for (String line : item.getLore())
                lore.add(BungeeUtils.formatBungeeServerString(
                        line.replaceAll("%player%", getPlayer().getName())
                ));
            set(item.getColumn(), item.getRow(), new Button(new ItemBuilder(item.getMaterial(), item.getAmount(), item.getData())
                    .setName(Style.color(item.getName()))
                    .setLore(Style.colorLines(lore)).toItemStack(), event -> {
                for (String action : item.getActions())
                    ActionManager.executeAction(getPlayer(), action);
            }));
        }
    }

    @Override
    public boolean onEvent(Event event) {
        if (event instanceof InventoryCloseEvent || event instanceof PlayerQuitEvent)
            menus.remove(getPlayer());
        return true;
    }

    public static Tuple<AtlasMenu, Menu> getOpenedMenu(Player player) {
        if (!menus.containsKey(player))
            return null;
        MenuProvider provider = menus.get(player);
        return new Tuple<>(provider, provider.menu);
    }
}
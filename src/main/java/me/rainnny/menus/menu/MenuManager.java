package me.rainnny.menus.menu;

import lombok.Getter;
import me.rainnny.api.annotation.Init;
import me.rainnny.api.menu.AtlasMenu;
import me.rainnny.api.util.*;
import me.rainnny.menus.AdvancedMenus;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.*;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
@Init(priority = Priority.HIGH)
public class MenuManager {
    @Getter private static final List<Menu> menus = new ArrayList<>();

    public MenuManager() {
        load();
    }

    @SuppressWarnings("unchecked")
    public static void load() {
        // This will create the new file (menus.yml) if it doesn't exist, and load it
        RainnnyFile file = new RainnnyFile(AdvancedMenus.INSTANCE, "menus.yml", null);
        FileConfiguration configuration = file.getConfiguration();
        ConfigurationSection section = configuration.getConfigurationSection("menus");
        if (section == null || (section.getKeys(false).isEmpty()))
            return;

        // Menu loading
        MiscUtils.printToConsole(Color.Yellow + "Loading menus...");
        for (String menuName : section.getKeys(false)) {
            String path = "menus." + menuName + ".";

            // Loading the menu
            List<String> commands = configuration.getStringList(path + "commands");
            String title = configuration.getString(path + "title");
            int size = configuration.getInt(path + "size");

            // Loading the menu items
            List<MenuItem> items = new ArrayList<>();
            ConfigurationSection itemsSection = configuration.getConfigurationSection(path + "items");
            if (itemsSection != null && (!itemsSection.getKeys(false).isEmpty())) {
                for (String itemID : itemsSection.getKeys(false)) {
                    Material material = Material.valueOf(configuration.get(path + "items." + itemID + ".type", "STONE").toString());
                    byte data = (byte) Integer.parseInt(configuration.get(path + "items." + itemID + ".data", 0).toString());
                    int amount = Integer.parseInt(configuration.get(path + "items." + itemID + ".amount", 1).toString());
                    int column = Integer.parseInt(configuration.get(path + "items." + itemID + ".slot.column", 0).toString());
                    int row = Integer.parseInt(configuration.get(path + "items." + itemID + ".slot.row", 0).toString());
                    String name = configuration.get(path + "items." + itemID + ".name", "").toString();
                    List<String> lore = (List<String>) configuration.getList(path + "items." + itemID + ".lore", new ArrayList<>());
                    List<String> actions = (List<String>) configuration.getList(path + "items." + itemID + ".actions", new ArrayList<>());

                    items.add(new MenuItem(itemID, material, data, amount, column, row, name, lore, actions));
                }
            }

            menus.add(new Menu(menuName, commands, title, size, items));
        }
        MiscUtils.printToConsole(Color.Green + "Loaded " + menus.size() + " menus!");
    }

    public static void reload() {
        // Loops through all the online players, and closes their menu if they have one open
        Arrays.stream(ServerUtils.getPlayers()).forEach(player -> {
            Tuple<AtlasMenu, Menu> menu = MenuProvider.getOpenedMenu(player);
            if (menu != null)
                menu.one.close();
        });

        // Clears the menus
        menus.forEach(menu -> {
            menu.getCommands().clear();
            menu.getItems().clear();
        });
        menus.clear();

        load();
    }
}
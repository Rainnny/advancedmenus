package me.rainnny.menus.menu;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
@AllArgsConstructor @Getter
public class Menu {
    private final String name;
    private final List<String> commands;

    private final String title;
    private final int size;
    private final List<MenuItem> items;
}
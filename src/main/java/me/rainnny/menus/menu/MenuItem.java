package me.rainnny.menus.menu;

import lombok.AllArgsConstructor;
import lombok.Getter;
import me.rainnny.menus.menu.action.ItemAction;
import org.bukkit.Material;

import java.util.List;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
@AllArgsConstructor @Getter
public class MenuItem {
    private final String id;

    private final Material material;
    private final byte data;
    private final int amount, column, row;
    private final String name;
    private final List<String> lore, actions;
}
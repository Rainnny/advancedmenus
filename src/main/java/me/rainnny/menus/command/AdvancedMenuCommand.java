package me.rainnny.menus.command;

import me.rainnny.api.annotation.Init;
import me.rainnny.api.command.Command;
import me.rainnny.api.command.CommandProvider;
import me.rainnny.api.util.Color;
import me.rainnny.api.util.Priority;
import me.rainnny.api.util.Style;
import me.rainnny.menus.menu.MenuManager;
import org.bukkit.command.CommandSender;

/*
 * Date Created: Apr 30, 2020
 *
 * @author Braydon
 *
 */
@Init(registerCommands = true, priority = Priority.LOW)
public class AdvancedMenuCommand {
    @Command(name = "advancedmenu", aliases = { "am" }, permission = "advancedmenu.command")
    public void onCommand(CommandProvider command) {
        CommandSender sender = command.getSender();
        String[] args = command.getArgs();

        if (args.length >= 1 && (args[0].equalsIgnoreCase("reload"))) {
            sender.sendMessage(Color.Gray + "Reloading...");
            MenuManager.reload();
            sender.sendMessage(Color.Green + "Reloaded!");
        } else sender.sendMessage(Style.getUsage("/" + command.getLabel() + " <reload>"));
    }
}
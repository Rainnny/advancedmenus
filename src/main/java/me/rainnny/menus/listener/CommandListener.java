package me.rainnny.menus.listener;

import me.rainnny.api.annotation.Init;
import me.rainnny.api.event.AtlasListener;
import me.rainnny.api.event.EventManager;
import me.rainnny.api.event.Listen;
import me.rainnny.api.event.impl.ServerCommandEvent;
import me.rainnny.menus.AdvancedMenus;
import me.rainnny.menus.menu.Menu;
import me.rainnny.menus.menu.MenuManager;
import me.rainnny.menus.menu.MenuProvider;
import org.bukkit.entity.Player;

/*
 * Date Created: Apr 29, 2020
 *
 * @author Braydon
 *
 */
@Init
public class CommandListener implements AtlasListener {
    public CommandListener() {
        EventManager.getInstance().registerListener(AdvancedMenus.INSTANCE, this);
    }

    @Listen
    public void onCommand(ServerCommandEvent event) {
        if (event.isConsole())
            return;
        Player player = (Player) event.getSender();
        for (Menu menu : MenuManager.getMenus()) {
            for (String command : menu.getCommands()) {
                if (event.getCommand().equalsIgnoreCase(command)) {
                    event.setCancelled(true);
                    new MenuProvider(player, menu.getTitle(), menu.getSize(), menu).open();
                    break;
                }
            }
        }
    }
}